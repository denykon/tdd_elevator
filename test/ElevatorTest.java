import org.jmock.Expectations;
import org.jmock.auto.Mock;
import org.jmock.integration.junit4.JUnitRuleMockery;
import org.jmock.lib.concurrent.Synchroniser;
import org.jmock.lib.legacy.ClassImposteriser;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import www.xxx.elevator.controllers.ElevatorController;
import www.xxx.elevator.beans.*;
import www.xxx.elevator.constants.Direction;
import www.xxx.elevator.constants.State;
import www.xxx.elevator.constants.TransportationState;
import www.xxx.elevator.containers.Container;
import www.xxx.elevator.utilits.Randomizer;

import java.lang.reflect.Method;
import java.util.*;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.*;
import static org.junit.Assert.assertTrue;
import static www.xxx.elevator.constants.InitData.ELEVATOR_CAPACITY;
import static www.xxx.elevator.constants.InitData.PASSENGERS_NUMBER;
import static www.xxx.elevator.constants.InitData.STOREYS_NUMBER;

/**
 * TESTS
 */
public class ElevatorTest {

    private Building building;

    /**
     * setup
     */
    @Before
    public final void setUp() {
        building = new Building();
    }

    /**
     * init Storeys Number Equals Value Of Configuration File
     */
    //@Ignore
    @Test
    public final void initStoreysNumberEqualsValueOfConfigurationFile() {
        final int storeyValue = 10;
        assertThat("Stories number is:", STOREYS_NUMBER, is(storeyValue));
    }

    /**
     * init Elevator Capacity Equals Value Of Configuration File
     */
    //@Ignore
    @Test
    public final void initElevatorCapacityEqualsValueOfConfigurationFile() {
        final int elevatorCapacityValue = 5;
        assertThat("Elevator capacity is:", ELEVATOR_CAPACITY, is(elevatorCapacityValue));
    }

    /**
     * init Passengers Number Equals Value Of Configuration File
     */
    //@Ignore
    @Test
    public final void initPassengersNumberEqualsValueOfConfigurationFile() {
        final int passengersNumberValue = 100;
        assertThat("Passengers number is:", PASSENGERS_NUMBER, is(passengersNumberValue));
    }

    /**
     * passengers Instance Number Equals Value Configuration File
     */
    //@Ignore
    @Test
    public final void passengersInstanceNumberEqualsValueConfigurationFile() {
        final int passengersNumber = building.getPassengers().length;
        assertThat("Passengers instance number is:", passengersNumber, is(PASSENGERS_NUMBER));
    }

    /**
     * storeys Instance Number Equals Value Configuration File
     */
    //@Ignore
    @Test
    public final void storeysInstanceNumberEqualsValueConfigurationFile() {
        final int storeysNumber = building.getStoreys().length;
        assertThat("Storeys instance number is:", storeysNumber, is(STOREYS_NUMBER));
    }

    /**
     * every storey Has DispatchStoryContainer
     */
    //@Ignore
    @Test
    public final void everyStoreyHasDispatchStoryContainer() {
        final Storey[] storeys = building.getStoreys();

        for (Storey storey : storeys) {
            assertThat("Storey have a dispatchStoryContainer:", storey.getDispatchStoreyContainer(),
                    is(notNullValue()));
        }
    }

    /**
     * every Storey Has ArrivalStoreyContainer
     */
    //@Ignore
    @Test
    public final void everyStoreyHasArrivalStoreyContainer() {
        final Storey[] storeys = building.getStoreys();
        for (Storey storey : storeys) {
            assertThat("Storey have an arrivalStoreyContainer:", storey.getArrivalStoreyContainer(),
                    is(notNullValue()));
        }
    }

    /**
     * randomizer Returns Random Value Except For Initial
     */
    @Test
    public final void randomizerReturnsRandomValueExceptForInitial() {
        int testValue = 5;
        for (int i = 0; i < 10000; i++) {
            assertThat("Randomizer returns random value except for initial:",
                    Randomizer.randomGenerate(testValue), is(not(testValue)));
        }
    }

    /**
     * passengers Destination Storey Number In Range Of Building Storeys
     */
    //@Ignore
    @Test
    public final void passengersDestinationStoreyNumberInRangeOfBuildingStoreys() {
        final Passenger[] passengers = building.getPassengers();
        for (Passenger passenger : passengers) {
            assertThat("Passenger destination store number is:", passenger.getDestinationStoreyNumber(),
                    is(both(greaterThanOrEqualTo(0)).and(lessThan(STOREYS_NUMBER))));
        }
    }

    /**
     * passengers Dispatch Storey Number Not Equals Destination Storey Number
     */
    //@Ignore
    @Test
    public final void passengersDispatchStoreyNumberNotEqualsDestinationStoreyNumber() {
        final Passenger[] passengers = building.getPassengers();
        for (Passenger passenger : passengers) {
            assertThat("Passenger destination store number is not equals dispatch storey number:",
                    passenger.getDestinationStoreyNumber(),
                    is(not(passenger.getDispatchStoreyNumber())));
        }
    }

    /**
     * each Passenger Is On Right Storey
     */
    //@Ignore
    @Test
    public final void eachPassengerIsOnRightStorey() {
        final Passenger[] passengers = building.getPassengers();
        final Storey[] storeys = building.getStoreys();
        for (Passenger passenger : passengers) {
            Container container = storeys[passenger.getDispatchStoreyNumber()].getDispatchStoreyContainer();
            assertTrue("Passenger is on the right storey", container.isContains(passenger));
        }
    }

    /**
     * each Passenger Has Right Direction
     */
    @Test
    public final void eachPassengerHasRightDirection() {
        final Passenger[] passengers = building.getPassengers();
        int destinationStorey;
        int dispatchStorey;
        Direction direction;
        for (Passenger passenger : passengers) {
            destinationStorey = passenger.getDestinationStoreyNumber();
            dispatchStorey = passenger.getDispatchStoreyNumber();
            direction = (destinationStorey > dispatchStorey) ? Direction.UP : Direction.DOWN;
            assertThat("Each passenger has right direction", passenger.getDirection(), is(direction));
        }
    }

    /**
     * each Storey Has Different Number
     */
    @Test
    public final void eachStoreyHasDifferentNumber() {
        Storey[] storeys = building.getStoreys();
        Set<Integer> storeysNumbers = new HashSet<Integer>(storeys.length);
        for (Storey storey : storeys) {
            storeysNumbers.add(storey.getStoreyNumber());
        }
        assertThat("Each storey has a different number", storeysNumbers.size(), is(storeys.length));
    }

    /**
     * every Passenger Has Different Id
     */
    //@Ignore
    @Test
    public final void everyPassengerHasDifferentId() {
        final Set<Integer> ids = new HashSet<>();
        final Passenger[] passengers = building.getPassengers();
        for (Passenger passenger : passengers) {
            ids.add(passenger.getId());
        }
        assertThat("Every passenger has a different id", ids.size(), is(passengers.length));
    }

    /**
     * each Passenger Has Transportation Task
     */
    //@Ignore
    @Test
    public final void eachPassengerHasTransportationTask() {
        final Passenger[] passengers = building.getPassengers();
        for (Passenger passenger : passengers) {
            assertThat("Passenger has transportation task:", passenger.getTransportationTask(),
                    is(notNullValue()));
        }
    }

    /**
     * each Passenger Has Launched Transportation Task
     */
    //@Ignore
    @Test
    public final void eachPassengerHasLaunchedTransportationTask() {
        building.startAllPassengers();
        final Passenger[] passengers = building.getPassengers();

        for (Passenger passenger : passengers) {
            assertThat("Passenger has launched transportation task:", passenger.getTransportationState(),
                    is(TransportationState.IN_PROGRESS));
        }
    }

    /**
     * elevator Moves From Floor To Floor
     */
    //@Ignore
    @Test
    public final void elevatorMovesFromFloorToFloor() {
        final ElevatorController elevatorController = building.getElevatorController();
        final int initialStoreyNumber = elevatorController.getCurrentStoreyNumber();
        try {
            Method moveElevatorRefl = ElevatorController.class.getDeclaredMethod("moveElevator");
            moveElevatorRefl.setAccessible(true);
            moveElevatorRefl.invoke(elevatorController);
        } catch (ReflectiveOperationException e) {
            e.printStackTrace();
        }

        assertThat("Elevator is moves:", elevatorController.getCurrentStoreyNumber(),
                is(not(initialStoreyNumber)));
    }

    /**
     * elevator Controller Moves Elevator In Range Of Storeys
     */
    //@Ignore
    @Test
    public final void elevatorControllerMovesElevatorInRangeOfStoreys() {
        final ElevatorController elevatorController = building.getElevatorController();
        final int doubleStoreyNumber = STOREYS_NUMBER * 2;
        List<Integer> storeyList = new ArrayList<>(doubleStoreyNumber);
        for (int i = 0; i < doubleStoreyNumber; i++) {
            try {
                Method setDirectionRefl = ElevatorController.class.getDeclaredMethod("setDirection");
                Method moveElevatorRefl = ElevatorController.class.getDeclaredMethod("moveElevator");
                setDirectionRefl.setAccessible(true);
                moveElevatorRefl.setAccessible(true);
                setDirectionRefl.invoke(elevatorController);
                moveElevatorRefl.invoke(elevatorController);
            } catch (ReflectiveOperationException e) {
                e.printStackTrace();
            }
            storeyList.add(elevatorController.getCurrentStoreyNumber());
        }
        for (int storeyNumber : storeyList) {
            assertThat("Current storey is in range of building storeys:", storeyNumber,
                    is(both(greaterThanOrEqualTo(0)).and(lessThan(STOREYS_NUMBER))));
        }
    }

    /**
     * TEST: each Passenger On Storey Is Notified And Changes His State
     */
    //@Ignore
    @Test
    public final void eachPassengerOnStoreyIsNotifiedAndChangesHisState() {
        final int storeyNumber = 1;
        building.startAllPassengers();
        Storey[] storeys = building.getStoreys();
        Container container = storeys[storeyNumber].getDispatchStoreyContainer();
        container.notifyAllPassengers();
        List<Passenger> passengers = container.getAllPassengers();

        try {
            Thread.sleep(100 * passengers.size());
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        Direction elevatorDirection = passengers.get(0).getElevatorController().getElevatorDirection();
        for (Passenger passenger : passengers) {
            if (passenger.getDirection().equals(elevatorDirection)) {
                assertThat("Notified passenger changed his state", passenger.getState(), is(State.WANTS_TO_GO));
            }
        }
    }

    /**
     * TEST: elevator Controller Is Let In All Possible Passengers And After Moves Them Out
     */
    //@Ignore
    @Test
    public final void elevatorControllerIsLetInAllPossiblePassengersAndAfterMovesThemOut() {
        Storey[] storeys = building.getStoreys();
        List<Passenger> passengers = storeys[0].getDispatchStoreyContainer().getAllPassengers();
        for (Passenger passenger : passengers) {
            passenger.setState(State.WANTS_TO_GO);
        }
        ElevatorController elevatorController = building.getElevatorController();
        int elevatorPassengersNumber = elevatorController.getElevator().getContainer().getAllPassengers().size();
        assertThat("Zero passengers in elevator:", elevatorPassengersNumber, is(0));

        try {
            Method landingRefl = ElevatorController.class.getDeclaredMethod("landing", List.class);
            Method disembarkationRefl = ElevatorController.class.getDeclaredMethod("disembarkation", List.class);
            landingRefl.setAccessible(true);
            disembarkationRefl.setAccessible(true);

            landingRefl.invoke(elevatorController, passengers);

            List<Passenger> elevatorPassengers = elevatorController.getElevator().getContainer().getAllPassengers();
            elevatorPassengersNumber = elevatorPassengers.size();

            assertThat("Passengers number in elevator equals storey passengers number or elevator capacity",
                    elevatorPassengers.size(), anyOf(equalTo(elevatorPassengersNumber), equalTo(ELEVATOR_CAPACITY)));
            for (Passenger passenger : elevatorPassengers) {
                passenger.setState(State.WANTS_TO_GO);
            }
            disembarkationRefl.invoke(elevatorController, elevatorPassengers);

            elevatorPassengersNumber = elevatorPassengers.size();

        } catch (ReflectiveOperationException e) {
            e.printStackTrace();
        }
        assertThat("Zero passengers in elevator:", elevatorPassengersNumber, is(0));
    }

    private Synchroniser synchroniser = new Synchroniser();

    /**
     * Creates context
     * setImposteriser - for class without interface
     * setThreadingPolicy - for multithreading
     */
    @Rule
    public final JUnitRuleMockery context = new JUnitRuleMockery() { {
        setImposteriser(ClassImposteriser.INSTANCE);
        setThreadingPolicy(synchroniser);
    } };

    @Mock
    private TransportationTask transportationTaskMock;

    /**
     *  TEST: each Passenger In Elevator Is Notified
     */
    @Test
    public final void eachPassengerInElevatorIsNotified() {
        final int elevatorPassengersNumber = 5;
        final Elevator elevator = building.getElevatorController().getElevator();
        final Container elevatorContainer = elevator.getContainer();
        List<Passenger> passengers = new ArrayList<Passenger>(elevatorPassengersNumber);
        for (int i = 0; i < elevatorPassengersNumber; i++) {
            passengers.add(new Passenger(building));
        }

        for (Passenger passenger : passengers) {
            passenger.setTransportationTask(transportationTaskMock);
            elevatorContainer.addPassenger(passenger);
        }

        context.checking(new Expectations() { {
            exactly(elevatorPassengersNumber).of(transportationTaskMock).wakeUp();
        } });

        elevatorContainer.notifyAllPassengers();
    }

    /**
     * Test: passenger is changes his state by transportationTask
     */
    @Test
    public final void passengerIsChangesHisStateByTransportationTask() {
        Passenger passenger = new Passenger(building);
        passenger.getElevatorController().setElevatorDirection(passenger.getDirection());
        passenger.setState(State.WAITING);
        passenger.getTransportationTask().changeState();
        assertThat("Passenger state is WANTS_TO_GO:", passenger.getState(), is(State.WANTS_TO_GO));

        passenger.setState(State.LIFTING);
        passenger.getElevatorController().setCurrentStoreyNumber(passenger.getDestinationStoreyNumber());
        passenger.getTransportationTask().changeState();
        assertThat("Passenger state is WANTS_TO_GO:", passenger.getState(), is(State.WANTS_TO_GO));

        passenger.setState(State.ARRIVED);
        passenger.getTransportationTask().changeState();
        assertThat("Passenger transportation state is completed:", passenger.getTransportationState(),
                is(TransportationState.COMPLETED));
    }
}
