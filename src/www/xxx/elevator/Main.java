package www.xxx.elevator;

import www.xxx.elevator.beans.Building;
import www.xxx.elevator.utilits.LiftValidator;

/**
 * TDD_Elevator
 * Main class
 */
public final class Main {

    private Main() {
    }

    /**
     * Program enter point
     * @param args program args
     */
    public static void main(final String[] args) {
        Building building = new Building();
        building.startAllPassengers();
        building.getElevatorController().startTransportation();
        new LiftValidator(building).test();

    }
}
