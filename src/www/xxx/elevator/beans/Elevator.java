package www.xxx.elevator.beans;

import www.xxx.elevator.containers.Container;
import www.xxx.elevator.containers.ElevatorContainer;

/**
 * Elevator
 */
public class Elevator {

    private Container container = new ElevatorContainer();

    /**
     * Elevator container
     * @return Container of elevator
     */
    public final Container getContainer() {
        return container;
    }
}
