package www.xxx.elevator.beans;

import www.xxx.elevator.controllers.ElevatorController;

import java.util.concurrent.CountDownLatch;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import static www.xxx.elevator.constants.InitData.*;

/**
 * TDD_Elevator Building
 */
public class Building {

    private Storey[] storeys;
    private Passenger[] passengers;
    private ElevatorController elevatorController;
    private CountDownLatch countDownLatch;

    /**
     * Building constructor
     */
    public Building() {
        this.storeys = new Storey[STOREYS_NUMBER];
        this.passengers = new Passenger[PASSENGERS_NUMBER];
        this.elevatorController = new ElevatorController(storeys);
        this.countDownLatch = new CountDownLatch(PASSENGERS_NUMBER);
        createStoreysInBuilding();
        fillBuildingByPassengers();
    }

    /**
     * Starts a passengers
     */
    public final void startAllPassengers() {
        ExecutorService main = Executors.newFixedThreadPool(PASSENGERS_NUMBER);
        for (Passenger passenger: passengers) {
            main.submit(passenger.getTransportationTask());
        }
        try {
            countDownLatch.await();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        main.shutdown();
    }


    private void createStoreysInBuilding() {
        for (int i = 0; i < STOREYS_NUMBER; i++) {
            storeys[i] = new Storey();
        }
    }

    private void fillBuildingByPassengers() {
        for (int i = 0; i < PASSENGERS_NUMBER; i++) {
            passengers[i] = new Passenger(this);
            storeys[passengers[i].getDispatchStoreyNumber()].getDispatchStoreyContainer().addPassenger(passengers[i]);
        }
    }

    /**
     * Getting storeys of building
     * @return Array of building storeys
     */
    public final Storey[] getStoreys() {
        return storeys;
    }

    /**
     * Getting passengers of building
     * @return Array of all building passengers
     */
    public final Passenger[] getPassengers() {
        return passengers;
    }

    /**
     * Getting elevator controller
     * @return Elevator controller
     */
    public final ElevatorController getElevatorController() {
        return elevatorController;
    }

    /**
     * Getting CountDownLatch
     * @return CountDownLatch
     */
    public final CountDownLatch getCountDownLatch() {
        return countDownLatch;
    }
}
