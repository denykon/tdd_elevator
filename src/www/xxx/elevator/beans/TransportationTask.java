package www.xxx.elevator.beans;

import www.xxx.elevator.controllers.ElevatorController;
import www.xxx.elevator.constants.Direction;
import www.xxx.elevator.constants.State;
import www.xxx.elevator.constants.TransportationState;

import java.util.concurrent.Callable;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

/**
 * Transportation Task
 */
public class TransportationTask implements Callable {

    private Passenger passenger;
    private Direction passengerDirection;
    private ElevatorController elevatorController;
    private CountDownLatch countDownLatch;

    private Lock lock = new ReentrantLock();
    private Condition move = lock.newCondition();

    /**
     * Constructor
     * @param passenger current passenger
     */
    public TransportationTask(final Passenger passenger) {
        this.passenger = passenger;
        this.passengerDirection = passenger.getDirection();
        this.elevatorController = passenger.getElevatorController();
        this.countDownLatch = passenger.getCountDownLatch();
    }

    @Override
    public final Object call() {
        passenger.setTransportationState(TransportationState.IN_PROGRESS);
        countDownLatch.countDown();

        while (TransportationState.IN_PROGRESS.equals(passenger.getTransportationState())) {
            lock.lock();
            try {
                move.await();
                changeState();
            } catch (InterruptedException e) {
                e.printStackTrace();
            } finally {
                lock.unlock();
            }
        }

        if (passenger.getTransportationState().equals(TransportationState.COMPLETED)) {
            elevatorController.getCompletedTasks().incrementAndGet();
        }

        return null;
    }

    /**
     * Passenger state changer
     */
    public final void changeState() {
        switch (passenger.getState()) {
            case WAITING:
                if (passengerDirection.equals(elevatorController.getElevatorDirection())) {
                    passenger.setState(State.WANTS_TO_GO);
                }
                break;
            case LIFTING:
                if (elevatorController.getCurrentStoreyNumber() == passenger.getDestinationStoreyNumber()) {
                    passenger.setState(State.WANTS_TO_GO);
                }
                break;
            case ARRIVED:
                passenger.setTransportationState(TransportationState.COMPLETED);
                break;
            default:
                break;
        }
    }

    /**
     * Wake up a current task
     */
    public void wakeUp() {
        lock.lock();
        try {
            move.signal();
        } finally {
            lock.unlock();
        }
    }
}
