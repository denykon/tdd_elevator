package www.xxx.elevator.beans;

import www.xxx.elevator.containers.ArrivalStoreyContainer;
import www.xxx.elevator.containers.Container;
import www.xxx.elevator.containers.DispatchStoryContainer;

/**
 * Storey
 */
public class Storey {

    private static int currentStorey = 0;

    private final int storeyNumber = currentStorey++;
    private Container dispatchStoryContainer = new DispatchStoryContainer();
    private Container arrivalStoreyContainer = new ArrivalStoreyContainer();

    /**
     * Getter for dispatch storey container
     * @return dispatch storey container
     */
    public final Container getDispatchStoreyContainer() {
        return dispatchStoryContainer;
    }

    /**
     * Getter for arrival storey container
     * @return arrival storey container
     */
    public final Container getArrivalStoreyContainer() {
        return arrivalStoreyContainer;
    }

    /**
     * Getter for storey number
     * @return current storey number
     */
    public final int getStoreyNumber() {
        return storeyNumber;
    }
}
