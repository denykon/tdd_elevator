package www.xxx.elevator.beans;

import www.xxx.elevator.controllers.ElevatorController;
import www.xxx.elevator.constants.Direction;
import www.xxx.elevator.constants.State;
import www.xxx.elevator.constants.TransportationState;
import www.xxx.elevator.utilits.Randomizer;

import java.util.concurrent.CountDownLatch;

/**
 * Passenger
 */
public class Passenger {

    private static int currentId = 1;

    private Building building;

    private int id;
    private int destinationStoreyNumber;
    private int dispatchStoreyNumber;
    private TransportationState transportationState;
    private TransportationTask transportationTask;
    private State state;
    private Direction direction;

    /**
     * Constructor
     * @param building current building
     */
    public Passenger(final Building building) {
        this.id = currentId++;
        this.dispatchStoreyNumber = Randomizer.randomGenerate();
        this.destinationStoreyNumber = Randomizer.randomGenerate(dispatchStoreyNumber);
        this.transportationState = TransportationState.NOT_STARTED;
        this.building = building;
        if (destinationStoreyNumber > dispatchStoreyNumber) {
            this.direction = Direction.UP;
        } else {
            this.direction = Direction.DOWN;
        }
        setTransportationTask(new TransportationTask(this));
    }

    /**
     * Passenger id
     * @return id
     */
    public final int getId() {
        return id;
    }

    /**
     * Passenger destination storey number
     * @return storey number
     */
    public final int getDestinationStoreyNumber() {
        return destinationStoreyNumber;
    }

    /**
     * Passenger dispatch storey number
     * @return storey number
     */
    public final int getDispatchStoreyNumber() {
        return dispatchStoreyNumber;
    }

    /**
     * Transportation state
     * @return state of transportation
     */
    public final TransportationState getTransportationState() {
        return transportationState;
    }

    /**
     * Transportation task
     * @return transportation task
     */
    public final TransportationTask getTransportationTask() {
        return transportationTask;
    }

    /**
     * State of passenger
     * @return current State
     */
    public final State getState() {
        return state;
    }

    /**
     * Passenger direction
     * @return direction
     */
    public final Direction getDirection() {
        return direction;
    }

    /**
     * Elevator controller
     * @return elevator controller
     */
    public final ElevatorController getElevatorController() {
        return building.getElevatorController();
    }

    /**
     * CountDownLatch
     * @return CountDownLatch
     */
    public final CountDownLatch getCountDownLatch() {
        return building.getCountDownLatch();
    }

    /**
     * Setter for TransportationState
     * @param transportationState - Transportation State
     */
    public final void setTransportationState(final TransportationState transportationState) {
        this.transportationState = transportationState;
    }

    /**
     * Setter for passenger state
     * @param state current state
     */
    public final void setState(final State state) {
        this.state = state;
    }

    /**
     * Setter for TransportationTask
     *@param task current TransportationTask
     */
    public final void setTransportationTask(final TransportationTask task) {
        this.transportationTask = task;
    }

}
