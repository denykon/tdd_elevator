package www.xxx.elevator.constants;

/**
 * Elevator or passenger directions
 */
public enum Direction {
    /**
     * up direction
     */
    UP,
    /**
     * down direction
     */
    DOWN
}
