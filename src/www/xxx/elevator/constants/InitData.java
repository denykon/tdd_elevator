package www.xxx.elevator.constants;

import www.xxx.elevator.utilits.ConfigReader;

/**
 * TDD_Elevator configuration data
 */
public final class InitData {

    private InitData() {
    }

    /**
     * Building storeys number
     */
    public static final int STOREYS_NUMBER = ConfigReader.INSTANCE.getStoreysNumber();
    /**
     * Building passengers number
     */
    public static final int PASSENGERS_NUMBER = ConfigReader.INSTANCE.getPassengersNumber();
    /**
     * The elevator capacity
     */
    public static final int ELEVATOR_CAPACITY = ConfigReader.INSTANCE.getElevatorCapacity();
}
