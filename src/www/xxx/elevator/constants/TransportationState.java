package www.xxx.elevator.constants;

/**
 * Transportation states
 */
public enum TransportationState {
    /**
     * the transportation task (thread) is not started
     */
    NOT_STARTED,
    /**
     * the transportation task (thread) is started
     */
    IN_PROGRESS,
    /**
     * the transportation task (thread) is compiled
     */
    COMPLETED,
    /**
     * the transportation task (thread) is aborted
     * (only in version with UI)
     */
    ABORTED
}
