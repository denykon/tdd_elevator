package www.xxx.elevator.constants;

/**
 * Passengers states
 */
public enum State {
    /**
     * waiting a notify of controller
     */
    WAITING,
    /**
     * lifting in elevator
     */
    LIFTING,
    /**
     * says to controller, wants to go in elevator
     * or go out from elevator
     */
    WANTS_TO_GO,
    /**
     * arrived to destination storey
     */
    ARRIVED
}
