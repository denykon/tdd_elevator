package www.xxx.elevator.utilits;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.Properties;

/**
 * TDD_Elevator
 * Created by Dzianis Kanavalau on 6/26/2015.
 */
public enum ConfigReader {
    /**
     * Config reader Singleton
     */
    INSTANCE;

    private static final String CONFIG_FILE = "src/configuration/config.properties";
    private static final String CONFIG_STORIES = "STORIES_NUMBER";
    private static final String CONFIG_ELEVATOR = "ELEVATOR_CAPACITY";
    private static final String CONFIG_PASSENGERS = "PASSENGERS_NUMBER";

    private Properties properties = new Properties();

    private int storiesNumber;
    private int elevatorCapacity;
    private int passengersNumber;

    ConfigReader() {
        configLoad();
    }

    private void configLoad() {
        try {
            properties.load(new FileInputStream(new File(CONFIG_FILE)));
        } catch (IOException e) {
            e.printStackTrace();
        }
        storiesNumber = Integer.parseInt(properties.getProperty(CONFIG_STORIES));
        if (storiesNumber < 2) {
            throw new IllegalArgumentException("Storey number in config file must be more than one");
        }
        elevatorCapacity = Integer.parseInt(properties.getProperty(CONFIG_ELEVATOR));
        if (elevatorCapacity < 1) {
            throw new IllegalArgumentException("Elevator capacity in config file must be more than zero");
        }
        passengersNumber = Integer.parseInt(properties.getProperty(CONFIG_PASSENGERS));
        if (passengersNumber < 1) {
            throw new IllegalArgumentException("Passengers number in config file must be more than zero");
        }
    }

    /**
     * Storeys number
     * @return building storeys number
     */
    public int getStoreysNumber() {
        return storiesNumber;
    }

    /**
     * Elevator capacity
     * @return building elevator capacity
     */
    public int getElevatorCapacity() {
        return elevatorCapacity;
    }

    /**
     * Passenger number
     * @return building passengers number
     */
    public int getPassengersNumber() {
        return passengersNumber;
    }
}
