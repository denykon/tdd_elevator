package www.xxx.elevator.utilits;

import org.apache.log4j.Logger;
import www.xxx.elevator.beans.Building;
import www.xxx.elevator.beans.Passenger;
import www.xxx.elevator.beans.Storey;
import www.xxx.elevator.constants.TransportationState;

import java.text.MessageFormat;
import java.util.List;

/**
 * TDD_Elevator validator
 */
public class LiftValidator {

    private static final Logger VERIFY_LOGGER = Logger.getLogger("verify");
    private static final String TEST_FAIL = "TEST_FAIL.";
    private static final String TEST_PASSED = "TEST_PASSED.";

    private Building building;

    /**
     * LiftValidator constructor
     * @param building current building
     */
    public LiftValidator(final Building building) {
        this.building = building;
    }

    private boolean allDispatchStoreyContainersAreEmpty() {
        VERIFY_LOGGER.info("TEST: ALL_DISPATCH_CONTAINERS_ARE_EMPTY:");
        boolean result;
        Storey[] storeys = building.getStoreys();
        for (Storey storey: storeys) {
            result = storey.getDispatchStoreyContainer().getAllPassengers().isEmpty();
            VERIFY_LOGGER.info(MessageFormat.format("STOREY_#{0} - {1}", storey.getStoreyNumber() + 1, result));
            if (!result) {
                VERIFY_LOGGER.info(TEST_FAIL);
                return false;
            }
        }
        VERIFY_LOGGER.info(TEST_PASSED);
        return true;
    }

    private boolean elevatorContainerIsEmpty() {
        VERIFY_LOGGER.info("TEST: ELEVATOR_CONTAINER_IS_EMPTY:");
        if (!building.getElevatorController().getElevator().getContainer().getAllPassengers().isEmpty()) {
            VERIFY_LOGGER.info(TEST_FAIL);
            return false;
        }
        VERIFY_LOGGER.info(TEST_PASSED);
        return true;
    }

    private boolean allTransportationTasksAreCompleted() {
        VERIFY_LOGGER.info("TEST: ALL_TRANSPORTATION_TASKS_ARE_COMPLETED:");
        Passenger[] passengers = building.getPassengers();
        boolean result;
        for (Passenger passenger: passengers) {
            result = TransportationState.COMPLETED.equals(passenger.getTransportationState());
            VERIFY_LOGGER.info(MessageFormat.format("Passenger_with_id {0} - {1}", passenger.getId(),
                    passenger.getTransportationState().name()));
            if (!result) {
                VERIFY_LOGGER.info(TEST_FAIL);
                return false;
            }
        }
        VERIFY_LOGGER.info(TEST_PASSED);
        return true;
    }

    private boolean allPassengersDestinationStoreyNumberEqualArrivalStoreyNumber() {
        VERIFY_LOGGER.info("TEST: ALL_PASSENGERS_DESTINATION_STOREY_NUMBER_EQUALS_ARRIVAL_STOREY_NUMBER:");
        Storey[] storeys = building.getStoreys();
        int destinationStoreyNumber;
        int storeyNumber;
        for (Storey storey: storeys) {
            List<Passenger> storeyPassengers = storey.getArrivalStoreyContainer().getAllPassengers();
            for (Passenger passenger: storeyPassengers) {
                destinationStoreyNumber = passenger.getDestinationStoreyNumber();
                storeyNumber = storey.getStoreyNumber();
                VERIFY_LOGGER.info(MessageFormat.
                        format("Passenger_with_id {0}: destination store[{1}] - current storey[{2}]",
                                passenger.getId(), destinationStoreyNumber + 1, storey.getStoreyNumber() + 1));
                if (destinationStoreyNumber != storeyNumber) {
                    VERIFY_LOGGER.info(TEST_FAIL);
                    return false;
                }
            }
        }
        VERIFY_LOGGER.info(TEST_PASSED);
        return true;
    }


    private boolean arrivalStoreyPassengersNumberEqualsPassengerNumber() {
        VERIFY_LOGGER.info("TEST: ARRIVAL_STOREY_PASSENGERS_NUMBER_EQUALS_PASSENGERS_NUMBER:");
        int passengersNumber = building.getPassengers().length;
        int arrivalStoreysPassengers = 0;
        Storey[] storeys = building.getStoreys();
        for (Storey storey: storeys) {
            arrivalStoreysPassengers += storey.getArrivalStoreyContainer().getAllPassengers().size();
        }
        VERIFY_LOGGER.info(MessageFormat.
                format("Passenger in building - {0}, passengers on the arrival storeys - {1}",
                        passengersNumber, arrivalStoreysPassengers));
        if (passengersNumber != arrivalStoreysPassengers) {
            VERIFY_LOGGER.info(TEST_FAIL);
            return false;
        }
        VERIFY_LOGGER.info(TEST_PASSED);
        return true;
    }

    /**
     * All tests runner
     */
    public final void test() {
        VERIFY_LOGGER.info("START_ELEVATOR_VALIDATION...");
        String test;
        if (allDispatchStoreyContainersAreEmpty()
                && elevatorContainerIsEmpty()
                && allTransportationTasksAreCompleted()
                && allPassengersDestinationStoreyNumberEqualArrivalStoreyNumber()
                && arrivalStoreyPassengersNumberEqualsPassengerNumber()
                ) {
            test = "PASSED.";
        } else {
            test = "FAILED";
        }
        VERIFY_LOGGER.info("END_ELEVATOR_VALIDATION...");
        VERIFY_LOGGER.info(MessageFormat.format("VALIDATION IS {0}", test));
    }
}
