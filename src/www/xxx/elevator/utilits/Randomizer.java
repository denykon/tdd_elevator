package www.xxx.elevator.utilits;

import static www.xxx.elevator.constants.InitData.*;

import java.util.Random;

/**
 * TDD_Elevator
 * Created by Dzianis Kanavalau on 6/26/2015.
 */
public final class Randomizer {

    private Randomizer() {
    }

    /**
     * generate a random storey number
     * @return random number
     */
    public static int randomGenerate() {
        return generate();
    }

    /**
     * generate a random storey number expecting current number
     * @param except expect number
     * @return random number
     */
    public static int randomGenerate(final int except) {
        int number = generate();
        while (except == number) {
            number = generate();
        }
        return number;
    }

    private static int generate() {
        return (new Random().nextInt(STOREYS_NUMBER));
    }
}
