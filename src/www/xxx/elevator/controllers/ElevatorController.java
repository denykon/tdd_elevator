package www.xxx.elevator.controllers;

import org.apache.log4j.Logger;
import www.xxx.elevator.beans.Elevator;
import www.xxx.elevator.beans.Passenger;
import www.xxx.elevator.beans.Storey;
import www.xxx.elevator.constants.Direction;
import www.xxx.elevator.constants.State;

import java.text.MessageFormat;
import java.util.Iterator;
import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;

import static www.xxx.elevator.constants.InitData.*;

/**
 * Elevator controller
 */
public class ElevatorController {

    private static final Logger LOGGER = Logger.getLogger(ElevatorController.class);

    /**
     * Number of completed tasks
     */
    private AtomicInteger completedTasks = new AtomicInteger(0);

    private int currentStoreyNumber;
    private Storey[] storeys;
    private Elevator elevator;
    private Direction elevatorDirection;

    /**
     * Elevator controller constructor
     * @param storeys Storeys of building
     */
    public ElevatorController(final Storey[] storeys) {
        this.currentStoreyNumber = 0;
        this.storeys = storeys;
        this.elevator = new Elevator();
        this.elevatorDirection = Direction.UP;
    }

    /**
     * Starts elevator transportation
     */
    public final void startTransportation() {
        LOGGER.info("STARTING_TRANSPORTATION");
        while (completedTasks.get() < PASSENGERS_NUMBER) {
            setDirection();

            List<Passenger> liftPassengers = elevator.getContainer().getAllPassengers();
            if (!liftPassengers.isEmpty()) {
                elevator.getContainer().notifyAllPassengers();
                waiting(50);
                disembarkation(liftPassengers);
            }

            List<Passenger> elevatorWaitingPassengers = storeys[currentStoreyNumber].
                    getDispatchStoreyContainer().getAllPassengers();
            if (!elevatorWaitingPassengers.isEmpty()) {
                storeys[currentStoreyNumber].getDispatchStoreyContainer().notifyAllPassengers();
                waiting(20);
                landing(elevatorWaitingPassengers);
            }

            if (completedTasks.get() < PASSENGERS_NUMBER) {
                moveElevator();
            }
        }

        LOGGER.info("COMPLETION_TRANSPORTATION");
    }

    private void landing(final List<Passenger> passengers) {
        Iterator<Passenger> iterator = passengers.iterator();
        while (iterator.hasNext()) {
            Passenger currentPassenger = iterator.next();
            if (elevator.getContainer().getAllPassengers().size() < 5
                    && State.WANTS_TO_GO.equals(currentPassenger.getState())) {
                iterator.remove();
                elevator.getContainer().addPassenger(currentPassenger);
                LOGGER.info(MessageFormat.format("BOARDING_OF_PASSENGER ({0} on storey {1})",
                        currentPassenger.getId(), currentStoreyNumber + 1));
            } else {
                currentPassenger.setState(State.WAITING);
            }
        }
    }

    private void disembarkation(final List<Passenger> passengers) {
        Iterator<Passenger> iterator = passengers.iterator();
        while (iterator.hasNext()) {
            Passenger currentPassenger = iterator.next();
            if (State.WANTS_TO_GO.equals(currentPassenger.getState())) {
                iterator.remove();
                storeys[currentStoreyNumber].getArrivalStoreyContainer().addPassenger(currentPassenger);
                LOGGER.info(MessageFormat.format("DEBOARDING_OF_PASSENGER ({0} on storey {1})",
                        currentPassenger.getId(), currentStoreyNumber + 1));
                currentPassenger.getTransportationTask().wakeUp();
            }
        }
    }

    private void waiting(final int millis) {
        try {
            Thread.sleep(millis);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    private void setDirection() {
        if (currentStoreyNumber < 1) {
            elevatorDirection = Direction.UP;
        }
        if (currentStoreyNumber == STOREYS_NUMBER - 1) {
            elevatorDirection = Direction.DOWN;
        }
    }

    private void moveElevator() {
        if (Direction.UP.equals(elevatorDirection)) {
            currentStoreyNumber++;
            LOGGER.info(MessageFormat.format("MOVING_ELEVATOR (from storey {0} to storey {1})",
                    currentStoreyNumber, currentStoreyNumber + 1));
        } else {
            currentStoreyNumber--;
            LOGGER.info(MessageFormat.format("MOVING_ELEVATOR (from storey {0} to storey {1})",
                    currentStoreyNumber + 2, currentStoreyNumber + 1));
        }
    }

    /**
     * Getting number of current storey
     * @return number of current storey
     */
    public final int getCurrentStoreyNumber() {
        return currentStoreyNumber;
    }

    /**
     * Getting buildings elevator
     * @return elevator
     */
    public final Elevator getElevator() {
        return elevator;
    }

    /**
     * Direction of elevator
     * @return elevator direction
     */
    public final Direction getElevatorDirection() {
        return elevatorDirection;
    }

    /**
     * Getting completed tasks number
     * @return completed tasks number
     */
    public final AtomicInteger getCompletedTasks() {
        return completedTasks;
    }

    /**
     * Setter for elevator direction
     * @param elevatorDirection direction
     */
    public final void setElevatorDirection(final Direction elevatorDirection) {
        this.elevatorDirection = elevatorDirection;
    }

    /**
     * Setter for current storey
     * @param currentStoreyNumber storey number
     */
    public final void setCurrentStoreyNumber(final int currentStoreyNumber) {
        this.currentStoreyNumber = currentStoreyNumber;
    }
}
