package www.xxx.elevator.containers;

import www.xxx.elevator.beans.Passenger;
import www.xxx.elevator.constants.State;

import java.util.ArrayList;
import java.util.List;

/**
 * Arrival container
 */
public class ArrivalStoreyContainer implements Container {

    private List<Passenger> container = new ArrayList<>();

    @Override
    public final List<Passenger> getAllPassengers() {
        return container;
    }

    @Override
    public final boolean isContains(final Passenger passenger) {
        return container.contains(passenger);
    }

    @Override
    public final void addPassenger(final Passenger passenger) {
        passenger.setState(State.ARRIVED);
        container.add(passenger);
    }

    @Override
    public final void notifyAllPassengers() {
        for (Passenger passenger: container) {
            passenger.getTransportationTask().wakeUp();
        }
    }
}
