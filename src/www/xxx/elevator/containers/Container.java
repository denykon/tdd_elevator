package www.xxx.elevator.containers;

import www.xxx.elevator.beans.Passenger;

import java.util.List;

/**
 * TDD_Elevator
 * Created by Dzianis Kanavalau on 6/26/2015.
 */
public interface Container {
    /**
     * get all passengers
     * @return list of all passengers in container
     */
    List<Passenger> getAllPassengers();

    /**
     * Is container contains passenger
     * @param passenger current passenger
     * @return true or false
     */
    boolean isContains(Passenger passenger);

    /**
     * Adds passenger to container
     * @param passenger current passenger
     */
    void addPassenger(Passenger passenger);

    /**
     * notifies all passengers in container
     * can go to elevator or go out to storey
     */
    void notifyAllPassengers();
}
