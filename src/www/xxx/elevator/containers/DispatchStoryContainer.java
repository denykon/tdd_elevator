package www.xxx.elevator.containers;

import www.xxx.elevator.beans.Passenger;
import www.xxx.elevator.constants.State;

import java.util.ArrayList;
import java.util.List;

/**
 * Dispatch container
 */
public class DispatchStoryContainer implements Container {

    private List<Passenger> container = new ArrayList<>();

    @Override
    public final List<Passenger> getAllPassengers() {
        return container;
    }

    @Override
    public final boolean isContains(final Passenger passenger) {
       return container.contains(passenger);
    }

    @Override
    public final void addPassenger(final Passenger passenger) {
        passenger.setState(State.WAITING);
        container.add(passenger);
    }

    @Override
    public final void notifyAllPassengers() {
        for (Passenger passenger: container) {
            passenger.getTransportationTask().wakeUp();
        }
    }
}
