***## # README # ##***

This repository contains elevator simulator.
Program created by Dzianis Kanavalau, using TDD method.

### ---------------------------- ###
* Elevator
* Version 1.0
### Configuration & Dependencies ###
* Configuration: to change gonfiguration, you may modify config.properties file
* Dependencies: for running you need log4j library, present in lib path.
### Compiling ###
Please, use build.xml for compiling current program.

![elevader.jpg](https://bitbucket.org/repo/bdna4z/images/1042227347-elevader.jpg)